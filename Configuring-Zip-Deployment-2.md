Zip deployment is the default deployment mechanism. In this flow, a zip archive of your source code is created and uploaded to Azure. By default, this includes your `node_modules` directory as well, which can be customized to only deploy the source and run the build command (`$ npm install` for node) on Azure.

> **TIP:** Update your app's deployment source to `None` to use Zip Deployment. Do this by right-clicking the app in the explorer then clicking "Configure Deployment Source…"

## Configure zip deployment

  * If you set the deployment source of your web app to “None” (the default source on app creation), the deploy command will Zip the contents of a selected folder and upload the Zip file to Azure.

  * VS Code will prompt on deploy if you would like to configure your project for faster deployment.  If you click 'Yes', the following changes will be made in your project:
    * The vscode setting `appService.zipIgnorePattern` is changed to indicate that build artifacts will not be zipped and deployed.  These will be built on the server by running the appropriate build command.
    * A '.deployment' file will be created in the root of the project.  This file configures `SCM_DO_BUILD_DURING_DEPLOYMENT=true` enabling build on deploy.
      > NOTE: Currently only 'node' runtimes support this feature.
  * If you select 'Never show again,' the `appService.showBuildDuringDeployPrompt` vscode setting will be set to `false` and you will no longer be prompted for this project.  Delete this setting or set it to `true` to re-enable the prompt.

### Additional Zip Deploy Configuration Settings

* `appService.deploySubpath`
  * The default subpath of a workspace folder to use when deploying.

* `appService.preDeployTask`
  * The name of the task to run before deploying.

* `appService.zipGlobPattern`
  * Uses a glob pattern to define which files to be included in the deployment. The default value is '**/*'.

* `appService.zipIgnorePattern`
  * Uses a glob pattern to define which files to be excluded from the deployment. The default value is [] which doesn’t exclude any files/folders.

* For example, you might want to exclude the 'node_modules' folder from the deployment to speed up the Zip file creation and uploading. In this case, you will need the following setting:
  * `"appService.zipIgnorePattern": ["node_modules{,/**}"]`
  * And in order to have the web app run the proper deployment command to install the npm packages, you need to have the following Application Setting on your site or in a '.deployment' file at the root of your repo:
    * `SCM_DO_BUILD_DURING_DEPLOYMENT=true`

  