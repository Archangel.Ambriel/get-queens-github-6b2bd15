Connect the Visual Studio Code debugger to your apps running on Azure App Service on Linux. The debugger works the same as when it's connected to a local Node.js process - including the use of Breakpoints and Logpoints.

## Supported runtimes
Currently, only Node.js >=8.11 on Linux is supported.

## Starting a remote debugging session
Once your app is deployed, right-click the app in the App Service explorer and select "Start Remote Debugging." This process requires that the app be restarted with the debugger enabled - you'll be prompted to confirm the restart.

![Start remote debugging](https://user-images.githubusercontent.com/1186948/50987284-0f077700-14be-11e9-97d8-0779581180ce.png)

Once restarted, Visual Studio Code will connect to the app's debugging port via an SSH tunnel. It may take a little time to establish the connection. Once connected, Visual Studio Code will switch into debugging mode and work the same as it does when debugging an app locally.

![Remote breakpoints](https://user-images.githubusercontent.com/1186948/50987340-424a0600-14be-11e9-87f6-12045e251650.png)

When you're ready to end your remote debugging session, disconnect from the debugger as you would normally and confirm that you want to restart the app.